#pragma once

#include "CoreMinimal.h"
#include "CellBase.h"
#include "UObject/NoExportTypes.h"
#include "WallsCell.generated.h"

UCLASS()
class PROCEDURALLABYRINTHGENERATOR_API UWallsCell : public UCellBase
{
	GENERATED_BODY()

public:
	UWallsCell();

	UFUNCTION()
	TArray<bool> GetWalls() const;

	UFUNCTION()
	void SetWalls(TArray<bool> NewWalls);

	UFUNCTION()
	void SetWall(uint8 Direction, bool IsWall);

	UFUNCTION()
	bool HasAnyWall() const;

	UFUNCTION()
	void SetUsed(bool IsUsed);

	UFUNCTION()
	bool IsUsed() const;

private:
	UPROPERTY()
	TArray<bool> Walls;

	UPROPERTY()
	bool bUsed;

};
