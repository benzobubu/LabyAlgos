#pragma once

#include "CoreMinimal.h"
#include "CellBase.h"
#include "GroundCellState.h"
#include "UObject/NoExportTypes.h"
#include "GroundCell.generated.h"

UCLASS()
class PROCEDURALLABYRINTHGENERATOR_API UGroundCell : public UCellBase
{
	GENERATED_BODY()

public:
	UGroundCell();

	bool IsFullyGrounded() const;

	bool HasAnyGround() const;

	UPROPERTY()
	FVector GridPosition;

	UPROPERTY()
	FVector WorldPosition;

	UPROPERTY()
	TArray<bool> GroundPoints;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="GroundStatus")
	TEnumAsByte<EGroundCellState> GroundState;

};
