#pragma once

UENUM()
enum ELabyrinthDirection
{
	Top		UMETA(DisplayName="Top"),
	Right	UMETA(DisplayName="Right"),
	Bottom	UMETA(DisplayName="Botton"),
	Left	UMETA(DisplayName="Left")
};