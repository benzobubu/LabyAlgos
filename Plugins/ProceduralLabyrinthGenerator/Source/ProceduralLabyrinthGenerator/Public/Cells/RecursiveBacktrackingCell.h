#pragma once

#include "RecursiveBacktrackingCell.generated.h"

UCLASS()
class PROCEDURALLABYRINTHGENERATOR_API URecursiveBacktrackingCell : public UObject
{
	GENERATED_BODY()

public:
	URecursiveBacktrackingCell();

	UFUNCTION()
	void SetVisited(bool IsVisited);

	UFUNCTION()
	bool IsVisited() const;

	UFUNCTION()
	void SetUsed(bool IsUsed);

	UFUNCTION()
	bool IsUsed() const;

	UFUNCTION()
	void RemoveWalls(URecursiveBacktrackingCell* Next);

	UFUNCTION()
	void RemoveAllWalls();

	UFUNCTION()
	TArray<bool> GetWalls() const;

	UFUNCTION()
	void SetPosition(FIntPoint NewPosition);

	UFUNCTION()
	FIntPoint GetPosition() const;

private:
	UPROPERTY()
	FIntPoint Position;

	UPROPERTY()
	bool bVisited;

	UPROPERTY()
	TArray<bool> Walls;

	UPROPERTY()
	bool bUsed;

};

