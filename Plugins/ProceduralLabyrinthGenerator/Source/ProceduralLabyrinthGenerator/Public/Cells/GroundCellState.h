#pragma once

UENUM()
enum EGroundCellState
{
	FullGround		UMETA (DisplayName = "FullGrounded"),
	PartialGround	UMETA (DisplayName = "PartiallyGrounded"),
	NoGround		UMETA (DisplayName = "NotGrounded")
};