#pragma once

#include "CoreMinimal.h"
#include "Graph/LabyrinthGraph.h"
#include "Grids/WallsGrid.h"
#include "UObject/Object.h"
#include "LabyrinthData.generated.h"

UCLASS()
class PROCEDURALLABYRINTHGENERATOR_API ULabyrinthData : public UObject
{
	GENERATED_BODY()

public:
	UFUNCTION()
	UWallsGrid* GetLabyrinthGrid() const;

	UFUNCTION()
	void SetLabyrinthGrid(UWallsGrid* Grid);

	UFUNCTION()
	ULabyrinthGraph* GetLabyrinthGraph() const;

	UFUNCTION()
	void SetLabyrinthGraph(ULabyrinthGraph* Graph);

protected:
	UPROPERTY()
	UWallsGrid* LabyrinthGrid;

	UPROPERTY()
	ULabyrinthGraph* LabyrinthGraph;
};
