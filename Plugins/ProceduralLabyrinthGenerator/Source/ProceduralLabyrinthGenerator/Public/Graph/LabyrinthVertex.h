#pragma once

#include "CoreMinimal.h"
#include "Cells/WallsCell.h"
#include "UObject/Object.h"
#include "LabyrinthVertex.generated.h"

UCLASS()
class PROCEDURALLABYRINTHGENERATOR_API ULabyrinthVertex : public UObject
{
	GENERATED_BODY()

public:
	UFUNCTION()
	void SetCell(UWallsCell* LabyrinthGridCell);

	UFUNCTION()
	UWallsCell* GetCell() const;

	UPROPERTY()
	TArray<ULabyrinthVertex*> Adjacents;

protected:
	UPROPERTY()
	UWallsCell* Cell;

};
