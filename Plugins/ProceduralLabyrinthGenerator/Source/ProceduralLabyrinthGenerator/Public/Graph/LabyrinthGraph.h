#pragma once

#include "CoreMinimal.h"
#include "LabyrinthVertex.h"
#include "Cells/WallsCell.h"
#include "UObject/Object.h"
#include "LabyrinthGraph.generated.h"

UCLASS()
class PROCEDURALLABYRINTHGENERATOR_API ULabyrinthGraph : public UObject
{
	GENERATED_BODY()

public:
	UFUNCTION()
	void AddVertex(ULabyrinthVertex* Vertex);

	UPROPERTY()
	TArray<ULabyrinthVertex*> Vertices;

	UFUNCTION()
	bool VertexCellExists(UWallsCell* Cell);

	UFUNCTION()
	ULabyrinthVertex* GetCellVertex(UWallsCell* Cell);

};
