#pragma once

#include "CoreMinimal.h"
#include "Graph/LabyrinthGraph.h"
#include "Grids/WallsGrid.h"
#include "LabyrinthUtilities.generated.h"

UCLASS()
class PROCEDURALLABYRINTHGENERATOR_API ULabyrinthUtilities : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	//Graphs
	static ULabyrinthGraph* GetGraphFromCell(UWallsGrid* Grid, UWallsCell* StartCell);

	static void RemoveAllWallsFromSamePathCells(UWallsGrid* Grid);

	static void RemoveDeadEndInteriorWalls(UWallsGrid* LabyrinthGrid);

	static void RemoveDeadEndInteriorWallsFromGraph(UWallsGrid* Grid, ULabyrinthGraph* Graph);

	static TArray<ULabyrinthGraph*> GetGraphsFromGrid(UWallsGrid* Grid);

	static TArray<ULabyrinthGraph*> SeparateGraphsAtNodes(ULabyrinthGraph* Graph);

	static TArray<ULabyrinthVertex*> GetNodes(ULabyrinthGraph* Graph);

	//Grid Section
	static UWallsGrid* GetLabyrinthGridSection(UWallsGrid* Grid, FIntPoint Position, FIntPoint Size);

	static void RemoveWallsFromGridSection(UWallsGrid* Grid, FIntPoint Position, FIntPoint Size);

	static bool IsGridSectionFullyUsed(UWallsGrid* Grid, FIntPoint Position, FIntPoint Size);

	//Outline
	static TArray<FIntPoint> GetExternalOutlineCells(UWallsGrid* Grid);

	static TArray<bool> GetCellExternalOpenings(UWallsGrid* Grid, FIntPoint Position);

	static TArray<bool> GetCellExternalWalls(UWallsGrid* Grid, FIntPoint Position);

	static TArray<bool> GetCellExternalOutlineSides(UWallsGrid* Grid, FIntPoint Position);

	//Cells
	static TArray<UWallsCell*> GetConnectedNeighborCells(UWallsGrid* Grid, UWallsCell* Cell);

	static TArray<UWallsCell*> GetSeparatedNeighborCells(UWallsGrid* Grid, UWallsCell* Cell);

	static FIntPoint GetPointFromLabyrinthDirection(uint8 Direction);

	static uint8 GetDirectionFromNeighborCells(const UWallsGrid* Grid, UWallsCell* Start, UWallsCell* End);

};
