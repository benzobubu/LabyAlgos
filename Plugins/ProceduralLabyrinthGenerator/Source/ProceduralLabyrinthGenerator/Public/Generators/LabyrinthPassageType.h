#pragma once

UENUM(BlueprintType)
enum class ELabyrinthPassageType : uint8
{
	Entrance	UMETA(DisplayName="Entrance"),
	Exit		UMETA(DisplayName="Exit"),
	Undefined	UMETA(DisplayName="Undefined")
};
