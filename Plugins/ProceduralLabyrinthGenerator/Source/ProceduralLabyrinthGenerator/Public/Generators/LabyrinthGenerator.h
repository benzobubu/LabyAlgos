#pragma once

#include "CoreMinimal.h"
#include "LabyrinthEntranceExitSelector.h"
#include "LabyrinthEnvironmentGeneratorBase.h"
#include "LabyrinthGridGenerator.h"
#include "GameFramework/Actor.h"
#include "Grids/GroundGrid.h"
#include "LabyrinthGenerator.generated.h"

UCLASS()
class PROCEDURALLABYRINTHGENERATOR_API ALabyrinthGenerator : public AActor
{
	GENERATED_BODY()

public:
	ALabyrinthGenerator();

	UFUNCTION()
	void GenerateLabyrinth();

	UFUNCTION()
	void SetSeed(int32 NewSeed);

	UFUNCTION()
	void SetLabyrinthArea(const TArray<FVector>& Vertices);

	UFUNCTION()
	void RemoveCurrentLabyrinth() const;

protected:
	UPROPERTY(EditDefaultsOnly, Category="Generator")
	TSubclassOf<ULabyrinthGridGenerator> LabyrinthGridGeneratorClass;

	UPROPERTY(EditInstanceOnly, Category="")
	ULabyrinthGridGenerator* LabyrinthGridGenerator;

	UPROPERTY(VisibleInstanceOnly, Category="")
	UWallsGrid* LabyrinthGrid;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Passages")
	TSubclassOf<ULabyrinthEntranceExitSelector> LabyrinthPassagesSelectorClass;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadWrite, Category="Passages")
	ULabyrinthEntranceExitSelector* LabyrinthPassageSelector;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Labyrinth|Environment")
	TSubclassOf<ALabyrinthEnvironmentGeneratorBase> LabyrinthEnvironmentGeneratorClass;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category="Labyrinth")
	ALabyrinthEnvironmentGeneratorBase* LabyrinthEnvironmentGenerator;

	UPROPERTY()
	int32 Seed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Labyrinth")
	float CellSize;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category="Ground")
	UGroundGrid* GroundGrid;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category="Labyrinth")
	TArray<FVector> LabyrinthArea;

	UFUNCTION(BlueprintCallable, Category="Initialization")
	void Initialize();

};
