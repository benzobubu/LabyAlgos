#pragma once

#include "CoreMinimal.h"
#include "Grids/WallsGrid.h"
#include "LabyrinthEnvironmentGeneratorBase.generated.h"

UCLASS()
class PROCEDURALLABYRINTHGENERATOR_API ALabyrinthEnvironmentGeneratorBase : public AActor
{
	GENERATED_BODY()

public:
	virtual void GenerateLabyrinthEnvironment(UWallsGrid* LabyrinthData) PURE_VIRTUAL(ALabyrinthEnvironmentGenerator::GenerateLabyrinthEnvironment,;);

	virtual void RemoveLabyrinthEnvironment() PURE_VIRTUAL(ALabyrinthEnvironmentGenerator::RemoveLabyrinthEnvironment,;);

};
