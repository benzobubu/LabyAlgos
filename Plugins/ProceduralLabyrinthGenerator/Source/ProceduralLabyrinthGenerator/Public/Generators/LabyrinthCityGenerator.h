#pragma once

#include "CoreMinimal.h"
#include "LabyrinthEnvironmentGeneratorBase.h"
#include "Buildings/BasicHouse.h"
#include "Buildings/LabyrinthWalls.h"
#include "Grids/WallsGrid.h"
#include "LabyrinthCityGenerator.generated.h"

UCLASS(Blueprintable, BlueprintType)
class PROCEDURALLABYRINTHGENERATOR_API ALabyrinthCityGenerator : public ALabyrinthEnvironmentGeneratorBase
{
	GENERATED_BODY()

public:
	ALabyrinthCityGenerator();

	virtual void GenerateLabyrinthEnvironment(UWallsGrid* LabyrinthGrid) override;

	virtual void RemoveLabyrinthEnvironment() override;

protected:
	UPROPERTY(EditDefaultsOnly, Category="Buildings")
	int BuildingsCount = 2;

	UPROPERTY(EditDefaultsOnly, Category="Buildings")
	TSubclassOf<ALabyrinthWalls> LabyrinthWallsClass;

	UPROPERTY(EditDefaultsOnly, Category="Buildings")
	TSubclassOf<ABasicHouse> BasicHouseClass;

	UFUNCTION()
	void SpawnBuilding(UWallsGrid* LabyrinthGrid, FIntPoint Position, FIntPoint Size) const;

};
