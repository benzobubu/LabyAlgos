#pragma once

#include "CoreMinimal.h"
#include "LabyrinthEntranceExitSelector.h"
#include "LabyrinthPassage.h"
#include "RandomEntranceAndExitSelector.generated.h"

UCLASS()
class PROCEDURALLABYRINTHGENERATOR_API URandomEntranceAndExitSelector : public ULabyrinthEntranceExitSelector
{
	GENERATED_BODY()

public:
	virtual  TArray<ULabyrinthPassage*> GetEntranceAndExitPoints(UWallsGrid* WallsGrid) override;

protected:
	FIntPoint GetRandomGridPoint(UWallsGrid* WallsGrid) const;
};
