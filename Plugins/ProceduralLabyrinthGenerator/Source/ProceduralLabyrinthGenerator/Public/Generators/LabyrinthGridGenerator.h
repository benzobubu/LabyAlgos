#pragma once

#include "CoreMinimal.h"
#include "Grids/GroundGrid.h"
#include "Grids/WallsGrid.h"
#include "LabyrinthGridGenerator.generated.h"

UCLASS(Abstract)
class PROCEDURALLABYRINTHGENERATOR_API ULabyrinthGridGenerator : public UObject
{
	GENERATED_BODY()

public:
	virtual UWallsGrid* Generate(UGroundGrid* WorldGrid, int Seed) PURE_VIRTUAL(ULabyrinthGridGenerator::Generate, return nullptr;);

protected:
	UPROPERTY()
	FRandomStream RandomStream;

};
