#pragma once
#include "LabyrinthGridGenerator.h"
#include "Cells/RecursiveBacktrackingCell.h"
#include "Grids/RecursiveBacktrackingGrid.h"
#include "Grids/WallsGrid.h"
#include "RecursiveBacktrackingGenerator.generated.h"

UCLASS()
class PROCEDURALLABYRINTHGENERATOR_API URecursiveBacktrackingGenerator : public ULabyrinthGridGenerator
{
	GENERATED_BODY()

public:
	virtual UWallsGrid* Generate(UGroundGrid* WorldGrid, int Seed) override;

protected:
	UPROPERTY()
	URecursiveBacktrackingGrid* Grid;

	UPROPERTY()
	URecursiveBacktrackingCell* Current;

	UPROPERTY()
	TArray<URecursiveBacktrackingCell*> Backtrace;

	UFUNCTION()
	URecursiveBacktrackingCell* FindNextCell() const;

	UFUNCTION()
	TArray<URecursiveBacktrackingCell*> GetAvailableNeighbors() const;

	UFUNCTION()
	int CalculateIndex(int Row, int Column) const;

};
