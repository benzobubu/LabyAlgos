#pragma once

#include "CoreMinimal.h"
#include "LabyrinthPassageType.h"
#include "UObject/Object.h"
#include "LabyrinthPassage.generated.h"

UCLASS(BlueprintType, Blueprintable)
class PROCEDURALLABYRINTHGENERATOR_API ULabyrinthPassage : public UObject
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category="Passage")
	ELabyrinthPassageType GetPassageType() const;

	UFUNCTION(BlueprintCallable, Category="Passage")
	void SetPassageType(ELabyrinthPassageType NewPassageType);

	UFUNCTION(BlueprintCallable, Category="Passage")
	FIntPoint GetGridPosition() const;

	UFUNCTION(BlueprintCallable, Category="Passage")
	void SetGridPosition(FIntPoint NewPosition);

protected:
	ELabyrinthPassageType PassageType;

	FIntPoint GridPosition;
};
