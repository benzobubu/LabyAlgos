#pragma once

#include "CoreMinimal.h"
#include "Cells/GroundCell.h"
#include "Grids/GroundGrid.h"

class PROCEDURALLABYRINTHGENERATOR_API UGroundGridGenerator : public UBlueprintFunctionLibrary
{
public:
	UGroundGridGenerator();

	static UGroundGrid* GenerateGroundGrid(TArray<FVector> Area, float CellSize, UObject* Outer);

	static TArray<bool> GetCellGroundInformation(TArray<FVector> PolygonVertices, UGroundCell* Cell, float CellSize);

private:
	static bool IsPositionInsideArea(TArray<FVector> Area, FVector Position);

};
