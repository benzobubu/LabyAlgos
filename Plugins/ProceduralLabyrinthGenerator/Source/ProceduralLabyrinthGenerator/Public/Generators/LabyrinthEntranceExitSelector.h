#pragma once

#include "CoreMinimal.h"
#include "LabyrinthPassage.h"
#include "Grids/WallsGrid.h"
#include "UObject/Object.h"
#include "LabyrinthEntranceExitSelector.generated.h"

UCLASS()
class PROCEDURALLABYRINTHGENERATOR_API ULabyrinthEntranceExitSelector : public UObject
{
	GENERATED_BODY()

public:
	virtual TArray<ULabyrinthPassage*> GetEntranceAndExitPoints(UWallsGrid* WallsGrid)
		PURE_VIRTUAL(ULabyrinthEntranceExitSelector::GetEntranceAndExitPoints, return TArray<ULabyrinthPassage*>(); );
};
