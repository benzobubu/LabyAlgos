#pragma once

#include "CoreMinimal.h"
#include "WorldGridBase.h"
#include "Cells/GroundCell.h"
#include "GroundGrid.generated.h"

UCLASS()
class PROCEDURALLABYRINTHGENERATOR_API UGroundGrid : public UWorldGridBase
{
	GENERATED_BODY()

public:
	virtual UGroundCell* GetCellAtIndex(int Index) override;

	virtual UGroundCell* GetCellAtGridPosition(FIntPoint Position) override;

};
