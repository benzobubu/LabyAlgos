#pragma once

#include "CoreMinimal.h"
#include "WorldGridBase.h"
#include "Cells/WallsCell.h"
#include "UObject/NoExportTypes.h"
#include "WallsGrid.generated.h"

UCLASS()
class PROCEDURALLABYRINTHGENERATOR_API UWallsGrid : public UWorldGridBase
{
	GENERATED_BODY()

public:
	virtual UWallsCell* GetCellAtGridPosition(FIntPoint Position) override;

	UFUNCTION()
	FIntPoint GetCellPosition(UWallsCell* Cell) const;

};
