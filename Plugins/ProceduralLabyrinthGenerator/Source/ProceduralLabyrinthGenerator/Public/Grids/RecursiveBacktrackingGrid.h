#pragma once
#include "Cells/RecursiveBacktrackingCell.h"

#include "RecursiveBacktrackingGrid.generated.h"

UCLASS()
class PROCEDURALLABYRINTHGENERATOR_API URecursiveBacktrackingGrid : public UObject
{
	GENERATED_BODY()

public:
	UFUNCTION()
	void SetSize(FIntPoint NewSize);

	UFUNCTION()
	FIntPoint GetSize() const;

	UPROPERTY()
	TArray<URecursiveBacktrackingCell*> Cells;

private:
	UPROPERTY()
	FIntPoint Size;

};
