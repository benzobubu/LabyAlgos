#pragma once

#include "Cells/CellBase.h"
#include "GridBase.generated.h"

UCLASS()
class PROCEDURALLABYRINTHGENERATOR_API UGridBase : public UObject
{
	GENERATED_BODY()

public:
	UFUNCTION()
	void SetGridSize(FIntPoint NewSize);

	UFUNCTION()
	FIntPoint GetGridSize() const;

	UFUNCTION(BlueprintCallable, Category="Cells")
	virtual UCellBase* GetCellAtGridPosition(FIntPoint Position);

	UFUNCTION(BlueprintCallable, Category="Cells")
	void AddCell(UCellBase* Cell);

	UFUNCTION(BlueprintCallable, Category="Cells")
	virtual UCellBase* GetCellAtIndex(int Index);

	UFUNCTION(BlueprintCallable, Category="Cells")
	int GetCellsNum() const;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Dimension")
	FIntPoint GridSize;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Cells")
	TArray<UCellBase*> Cells;

	UFUNCTION()
	int GetCellIndexAtPosition(FIntPoint Position) const;

	UFUNCTION()
	FIntPoint GetCellPositionAtIndex(int Index) const;

};
