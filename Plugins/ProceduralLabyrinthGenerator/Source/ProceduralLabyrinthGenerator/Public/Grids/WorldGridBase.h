#pragma once

#include "CoreMinimal.h"
#include "GridBase.h"
#include "WorldGridBase.generated.h"

UCLASS()
class PROCEDURALLABYRINTHGENERATOR_API UWorldGridBase : public UGridBase
{
	GENERATED_BODY()

public:
	UFUNCTION()
	FVector GetStartPosition() const;

	UFUNCTION()
	void SetStartPosition(FVector Position);

	UFUNCTION()
	FVector GetCellWorldPosition(FIntPoint GridPosition) const;

	UFUNCTION()
	float GetCellSize() const;

	UFUNCTION()
	void SetCellSize(float Size);

	UFUNCTION()
	void SetWorldSize(FVector Size);

	UFUNCTION()
	FVector GetWorldSize();

protected:
	UPROPERTY()
	FVector StartPosition;

	UPROPERTY()
	float CellSize;

	UPROPERTY()
	FVector WorldSize;

};
