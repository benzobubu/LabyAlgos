#pragma once
#include "CoreMinimal.h"
#include "BuildingBase.h"
#include "Grids/WallsGrid.h"
#include "LabyrinthWalls.generated.h"

UCLASS()
class PROCEDURALLABYRINTHGENERATOR_API ALabyrinthWalls : public ABuildingBase
{
	GENERATED_BODY()

public:
	ALabyrinthWalls();

	UPROPERTY(EditAnywhere, Category="Walls")
	UInstancedStaticMeshComponent* Walls;

	UFUNCTION(BlueprintCallable, Category="Building")
	virtual void Build() override;

	UFUNCTION()
	void SetLabyrinthGrid(UWallsGrid* Grid);

protected:
	UPROPERTY(VisibleInstanceOnly, Category="Grid")
	UWallsGrid* LabyrinthGrid;

	UPROPERTY(EditDefaultsOnly, Category="Walls")
	UStaticMesh* WallMesh;

	UFUNCTION()
	void SpawnWalls(UWallsGrid* Grid) const;
};
