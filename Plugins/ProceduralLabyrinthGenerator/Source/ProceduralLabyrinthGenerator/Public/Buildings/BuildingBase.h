#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BuildingBase.generated.h"

UCLASS()
class PROCEDURALLABYRINTHGENERATOR_API ABuildingBase : public AActor
{
	GENERATED_BODY()

public:
	virtual void Build() PURE_VIRTUAL(ABuildingBase::Build,;);

};
