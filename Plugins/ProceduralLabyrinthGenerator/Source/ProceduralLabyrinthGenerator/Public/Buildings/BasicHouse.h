#pragma once

#include "CoreMinimal.h"
#include "BuildingBase.h"
#include "Grids/WallsGrid.h"
#include "BasicHouse.generated.h"

UCLASS()
class PROCEDURALLABYRINTHGENERATOR_API ABasicHouse : public ABuildingBase
{
	GENERATED_BODY()

public:
	ABasicHouse();

	UFUNCTION(BlueprintCallable, Category="Building")
	virtual void Build() override;

	UFUNCTION(BlueprintCallable, Category="Grid")
	void SetWallsGrid(UWallsGrid* Grid);

protected:
	UPROPERTY(VisibleInstanceOnly, Category="Grid")
	UWallsGrid* LabyrinthGrid;

	UPROPERTY(EditDefaultsOnly, Category="Dimensions")
	float RoofHeight = 500.f;

	UPROPERTY(EditDefaultsOnly, Category="Dimensions")
	float CellSize = 250.f;

	UPROPERTY(EditDefaultsOnly, Category="Meshes")
	UStaticMesh* WallMesh;

	UPROPERTY(EditDefaultsOnly, Category="Meshes")
	UStaticMesh* FloorMesh;

	UPROPERTY(EditDefaultsOnly, Category="Meshes")
	UStaticMesh* RoofMesh;

	UPROPERTY(EditDefaultsOnly, Category="Meshes")
	UStaticMesh* Door;

	UPROPERTY(VisibleInstanceOnly, Category="Mesh Instances")
	UInstancedStaticMeshComponent* FloorInstancedStaticMesheComp;

	UPROPERTY(VisibleInstanceOnly, Category="Mesh Instances")
	UInstancedStaticMeshComponent* RoofInstancedStaticMeshComp;

	UPROPERTY(VisibleInstanceOnly, Category="Mesh Instances")
	UInstancedStaticMeshComponent* WallInstancedStaticMeshComp;

	UPROPERTY(VisibleInstanceOnly, Category="Mesh Instances")
	UInstancedStaticMeshComponent* DoorInstancedStaticMeshComp;

	UFUNCTION()
	void BuildFloor() const;

	UFUNCTION()
	void BuildWalls() const;

	UFUNCTION()
	void BuildRoof() const;

	UFUNCTION()
	void BuildDoors() const;

	UFUNCTION()
	void BuildExternalWalls() const;

};
