#include "Cells/RecursiveBacktrackingCell.h"
#include "Cells/RecursiveBacktrackingDirectionEnum.h"

URecursiveBacktrackingCell::URecursiveBacktrackingCell()
{
	Walls.SetNum(4);
	for(bool& Wall: Walls)
	{
		Wall = true;
	}
}

void URecursiveBacktrackingCell::SetVisited(bool IsVisited)
{
	bVisited = IsVisited;
}

bool URecursiveBacktrackingCell::IsVisited() const
{
	return bVisited;
}

void URecursiveBacktrackingCell::SetUsed(bool IsUsed)
{
	bUsed = IsUsed;
}

bool URecursiveBacktrackingCell::IsUsed() const
{
	return bUsed;
}

void URecursiveBacktrackingCell::RemoveWalls(URecursiveBacktrackingCell* Next)
{
	const int x = Position.X - Next->GetPosition().X;
	const int y = Position.Y - Next->GetPosition().Y;

	if (x == 1)
	{
		Walls[Left] = false;
		Next->Walls[Right] = false;
	}
	else if (x == -1)
	{
		Walls[Right] = false;
		Next->Walls[Left] = false;
	}
	else if (y == 1)
	{
		Walls[Bottom] = false;
		Next->Walls[Top] = false;
	}
	else if (y == -1)
	{
		Walls[Top] = false;
		Next->Walls[Bottom] = false;
	}
}

void URecursiveBacktrackingCell::RemoveAllWalls()
{
	for(bool &Wall: Walls)
	{
		Wall = false;
	}
}

TArray<bool> URecursiveBacktrackingCell::GetWalls() const
{
	return Walls;
}

void URecursiveBacktrackingCell::SetPosition(FIntPoint NewPosition)
{
	Position = NewPosition;
}

FIntPoint URecursiveBacktrackingCell::GetPosition() const
{
	return Position;
}
