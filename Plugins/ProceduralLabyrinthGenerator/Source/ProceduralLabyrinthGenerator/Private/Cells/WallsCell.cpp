#include "Cells/WallsCell.h"

UWallsCell::UWallsCell()
{
}

TArray<bool> UWallsCell::GetWalls() const
{
	return Walls;
}

void UWallsCell::SetWalls(TArray<bool> NewWalls)
{
	Walls = NewWalls;
}

void UWallsCell::SetWall(const uint8 Direction, const bool IsWall)
{
	Walls[Direction] = IsWall;
}

bool UWallsCell::HasAnyWall() const
{
	for(bool HasWall: Walls)
	{
		if (HasWall)
		{
			return true;
		}
	}

	return false;
}

void UWallsCell::SetUsed(bool IsUsed)
{
	bUsed = IsUsed;
}

bool UWallsCell::IsUsed() const
{
	return bUsed;
}
