#include "Cells/GroundCell.h"

UGroundCell::UGroundCell()
{
}

bool UGroundCell::IsFullyGrounded() const
{
	for(const bool GroundPosition: GroundPoints)
	{
		if(!GroundPosition)
		{
			return false;
		}
	}

	return true;
}

bool UGroundCell::HasAnyGround() const
{
	for(const bool GroundPosition: GroundPoints)
	{
		if(GroundPosition)
		{
			return true;
		}
	}

	return false;
}
