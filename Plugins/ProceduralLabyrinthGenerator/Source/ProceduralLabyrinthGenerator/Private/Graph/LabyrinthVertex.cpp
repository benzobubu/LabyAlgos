#include "Graph/LabyrinthVertex.h"

void ULabyrinthVertex::SetCell(UWallsCell* LabyrinthGridCell)
{
	Cell = LabyrinthGridCell;
}

UWallsCell* ULabyrinthVertex::GetCell() const
{
	return Cell;
}
