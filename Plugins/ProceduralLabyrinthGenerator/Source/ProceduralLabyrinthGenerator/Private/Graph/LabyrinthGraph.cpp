#include "Graph/LabyrinthGraph.h"

void ULabyrinthGraph::AddVertex(ULabyrinthVertex* Vertex)
{
	Vertices.Add(Vertex);
}

bool ULabyrinthGraph::VertexCellExists(UWallsCell* Cell)
{
	for (const ULabyrinthVertex* Vertex : Vertices)
	{
		if (Vertex->GetCell() == Cell)
		{
			return true;
		}
	}

	return false;
}

ULabyrinthVertex* ULabyrinthGraph::GetCellVertex(UWallsCell* Cell)
{
	for (ULabyrinthVertex* Vertex : Vertices)
	{
		if (Vertex->GetCell() == Cell)
		{
			return Vertex;
		}
	}

	return nullptr;
}

