#include "Data/LabyrinthData.h"
#include "Grids/WallsGrid.h"

UWallsGrid* ULabyrinthData::GetLabyrinthGrid() const
{
	return LabyrinthGrid;
}

void ULabyrinthData::SetLabyrinthGrid(UWallsGrid* Grid)
{
	LabyrinthGrid = Grid;
}

ULabyrinthGraph* ULabyrinthData::GetLabyrinthGraph() const
{
	return LabyrinthGraph;
}

void ULabyrinthData::SetLabyrinthGraph(ULabyrinthGraph* Graph)
{
	LabyrinthGraph = Graph;
}
