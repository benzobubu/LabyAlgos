#include "Generators/LabyrinthPassage.h"
#include "Generators/LabyrinthPassageType.h"

ELabyrinthPassageType ULabyrinthPassage::GetPassageType() const
{
	return PassageType;
}

void ULabyrinthPassage::SetPassageType(const ELabyrinthPassageType NewPassageType)
{
	PassageType = NewPassageType;
}

FIntPoint ULabyrinthPassage::GetGridPosition() const
{
	return GridPosition;
}

void ULabyrinthPassage::SetGridPosition(const FIntPoint NewPosition)
{
	GridPosition = NewPosition;
}
