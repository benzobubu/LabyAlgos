#include "Generators/RecursiveBacktrackingGenerator.h"
#include "Grids/WallsGrid.h"

UWallsGrid* URecursiveBacktrackingGenerator::Generate(UGroundGrid* GroundGrid, const int Seed)
{
	RandomStream.Initialize(Seed);

	Grid = NewObject<URecursiveBacktrackingGrid>(this);
	Grid->SetSize(GroundGrid->GetGridSize());

	//Create Cells
	for(int i = 0; i < Grid->GetSize().X; i ++)
	{
		for(int j = 0; j < Grid->GetSize().Y; j ++)
		{
			URecursiveBacktrackingCell* Cell = NewObject<URecursiveBacktrackingCell>();
			Cell->SetPosition(FIntPoint(i, j));

			const UGroundCell* CurrentGroundCell = GroundGrid->GetCellAtGridPosition(FIntPoint(i,j));
			if(!CurrentGroundCell->IsFullyGrounded())
			{
				Cell->SetVisited(true);
				Cell->RemoveAllWalls();
				Cell->SetUsed(false);
			}
			else
			{
				Cell->SetUsed(true);
			}
			Grid->Cells.Add(Cell);
		}
	}

	//Find the first used cell
	int StartIndex;
	do
	{
		StartIndex = RandomStream.RandRange(0, Grid->Cells.Num() - 1);
		Current = Grid->Cells[StartIndex];
	}
	while (!GroundGrid->GetCellAtIndex(StartIndex)->IsFullyGrounded());

	//Generate the Labyrinth
	while(true)
	{
		Current->SetVisited(true);
		URecursiveBacktrackingCell* Next = FindNextCell();
		if (Next != nullptr)
		{
			Next->SetVisited(true);
			Backtrace.Add(Current);
			Current->RemoveWalls(Next);
			Current = Next;
		}
		else if (Backtrace.Num() > 0)
		{
			Current = Backtrace.Top();
			Backtrace.Pop();
		}
		else if (Backtrace.Num() == 0)
		{
			break;
		}
	}

	//Convert the RecursiveBacktrackingGrid to LabyrinthGrid before returning it
	UWallsGrid* LabyrinthGrid = NewObject<UWallsGrid>(this);
	LabyrinthGrid->SetStartPosition(GroundGrid->GetStartPosition());
	LabyrinthGrid->SetCellSize(GroundGrid->GetCellSize());
	LabyrinthGrid->SetGridSize(Grid->GetSize());
	for(const URecursiveBacktrackingCell* RecursiveBacktrackingCell: Grid->Cells)
	{
		UWallsCell* LabyrinthGridCell = NewObject<UWallsCell>();
		LabyrinthGridCell->SetWalls(RecursiveBacktrackingCell->GetWalls());
		LabyrinthGridCell->SetUsed(RecursiveBacktrackingCell->IsUsed());
		LabyrinthGrid->AddCell(LabyrinthGridCell);
	}

	return LabyrinthGrid;
}

URecursiveBacktrackingCell* URecursiveBacktrackingGenerator::FindNextCell() const
{
	TArray<URecursiveBacktrackingCell*> AvailableNeighbors = GetAvailableNeighbors();
	if (AvailableNeighbors.Num() > 0)
	{
		return AvailableNeighbors[RandomStream.RandRange(0, AvailableNeighbors.Num() - 1)];
	}

	return nullptr;
}

TArray<URecursiveBacktrackingCell*> URecursiveBacktrackingGenerator::GetAvailableNeighbors() const
{
	const FIntPoint CurrentPos = Current->GetPosition();
	int NeighborIndexes[4] =
	{
		CalculateIndex(CurrentPos.Y + 1, CurrentPos.X),
		CalculateIndex(CurrentPos.Y, CurrentPos.X + 1),
		CalculateIndex(CurrentPos.Y - 1, CurrentPos.X),
		CalculateIndex(CurrentPos.Y, CurrentPos.X - 1),
	};

	TArray<URecursiveBacktrackingCell*> Neighbors;
	for (int i : NeighborIndexes)
	{
		if (i != -1 && Grid->Cells[i]->IsUsed() && !Grid->Cells[i]->IsVisited())
		{
			Neighbors.Add(Grid->Cells[i]);
		}
	}

	return Neighbors;
}

int URecursiveBacktrackingGenerator::CalculateIndex(const int Row, const int Column) const
{
	const FIntPoint GridSize = Grid->GetSize();

	if (Row < 0 || Column < 0 || Column > GridSize.X - 1 || Row > GridSize.Y - 1)
	{
		return -1;
	}

	return Column * GridSize.Y + Row;
}
