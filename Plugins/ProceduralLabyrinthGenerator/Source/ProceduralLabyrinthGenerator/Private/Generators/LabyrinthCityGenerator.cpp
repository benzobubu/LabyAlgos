#include "Generators/LabyrinthCityGenerator.h"
#include "EngineUtils.h"
#include "Utilities/LabyrinthUtilities.h"

ALabyrinthCityGenerator::ALabyrinthCityGenerator()
{
}

void ALabyrinthCityGenerator::GenerateLabyrinthEnvironment(UWallsGrid* LabyrinthGrid)
{
	ULabyrinthUtilities::RemoveDeadEndInteriorWalls(LabyrinthGrid);

	for(int i = 0; i < BuildingsCount; i ++)
	{
		FIntPoint Size;
		FIntPoint Position;
		do
		{
			Size = FIntPoint (FMath::RandRange(1, 7), FMath::RandRange(1, 7));
			Position = FIntPoint (FMath::RandRange(0, LabyrinthGrid->GetGridSize().X - Size.X),
								  FMath::RandRange(0, LabyrinthGrid->GetGridSize().Y - Size.Y));
		}
		while (!ULabyrinthUtilities::IsGridSectionFullyUsed(LabyrinthGrid, Position, Size));
		SpawnBuilding(LabyrinthGrid, Position, Size);
	}

	if(LabyrinthWallsClass != nullptr)
	{
		ALabyrinthWalls* LabyrinthWalls = GetWorld()->SpawnActor<ALabyrinthWalls>(LabyrinthWallsClass, FTransform::Identity);
		if(LabyrinthWalls != nullptr)
		{
			LabyrinthWalls->SetLabyrinthGrid(LabyrinthGrid);
			LabyrinthWalls->Build();
		}
	}
}

void ALabyrinthCityGenerator::RemoveLabyrinthEnvironment()
{
	for (TActorIterator<ABuildingBase> ActorIterator(GetWorld()); ActorIterator; ++ActorIterator)
	{
		ActorIterator->Destroy();
	}
}

void ALabyrinthCityGenerator::SpawnBuilding(UWallsGrid* LabyrinthGrid, const FIntPoint Position, const FIntPoint Size) const
{
	UWallsGrid* BuildingGrid = ULabyrinthUtilities::GetLabyrinthGridSection(LabyrinthGrid, Position, Size);
	if(BasicHouseClass != nullptr && BuildingGrid != nullptr)
	{
		ULabyrinthUtilities::RemoveWallsFromGridSection(LabyrinthGrid, Position, Size);
		ABasicHouse* BasicHouse = GetWorld()->SpawnActor<ABasicHouse>(BasicHouseClass, FTransform::Identity);
		if(BasicHouse != nullptr)
		{
			BasicHouse->SetWallsGrid(BuildingGrid);
			BasicHouse->Build();
		}
	}
}