#include "Generators/LabyrinthGenerator.h"

#include "Generators/GroundGridGenerator.h"

ALabyrinthGenerator::ALabyrinthGenerator()
{
	Initialize();
}

void ALabyrinthGenerator::GenerateLabyrinth()
{
	Initialize();

	RemoveCurrentLabyrinth();

	if (LabyrinthGridGenerator)
	{
		if (LabyrinthArea.Num() > 0)
		{
			GroundGrid = UGroundGridGenerator::GenerateGroundGrid(LabyrinthArea, CellSize, this);
			if (GroundGrid != nullptr)
			{
				LabyrinthGrid = LabyrinthGridGenerator->Generate(GroundGrid, Seed);
				if (LabyrinthGrid != nullptr)
				{
					if (LabyrinthPassageSelector != nullptr)
					{
						LabyrinthPassageSelector->GetEntranceAndExitPoints(LabyrinthGrid);
					}
					LabyrinthEnvironmentGenerator->GenerateLabyrinthEnvironment(LabyrinthGrid);
				}
			}
		}
	}
}

void ALabyrinthGenerator::SetSeed(const int32 NewSeed)
{
	Seed = NewSeed;
}

void ALabyrinthGenerator::SetLabyrinthArea(const TArray<FVector>& Vertices)
{
	LabyrinthArea = Vertices;
}

void ALabyrinthGenerator::Initialize()
{
	if(LabyrinthGridGenerator == nullptr && LabyrinthGridGeneratorClass != nullptr)
	{
		LabyrinthGridGenerator = NewObject<ULabyrinthGridGenerator>(this, LabyrinthGridGeneratorClass);
	}

	if (LabyrinthEnvironmentGenerator == nullptr && LabyrinthEnvironmentGeneratorClass != nullptr)
	{
		LabyrinthEnvironmentGenerator = GetWorld()->SpawnActor<ALabyrinthEnvironmentGeneratorBase>(LabyrinthEnvironmentGeneratorClass, FTransform::Identity);
	}

	if (LabyrinthPassagesSelectorClass != nullptr && LabyrinthPassageSelector == nullptr)
	{
		LabyrinthPassageSelector = NewObject<ULabyrinthEntranceExitSelector>(this, LabyrinthPassagesSelectorClass);
	}
}

void ALabyrinthGenerator::RemoveCurrentLabyrinth() const
{
	if (LabyrinthEnvironmentGenerator != nullptr)
	{
		LabyrinthEnvironmentGenerator->RemoveLabyrinthEnvironment();
	}
}
