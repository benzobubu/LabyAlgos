#include "Generators/RandomEntranceAndExitSelector.h"
#include "Generators/LabyrinthPassage.h"
#include "PhysicsEngine/PhysicsSettings.h"

TArray<ULabyrinthPassage*> URandomEntranceAndExitSelector::GetEntranceAndExitPoints(UWallsGrid* WallsGrid)
{
	ULabyrinthPassage* Entrance = NewObject<ULabyrinthPassage>(WallsGrid);
	Entrance->SetPassageType(ELabyrinthPassageType::Entrance);
	Entrance->SetGridPosition(GetRandomGridPoint(WallsGrid));

	FIntPoint ExitPoint = FIntPoint::ZeroValue;
	while (ExitPoint == FIntPoint::ZeroValue || ExitPoint == Entrance->GetGridPosition())
	{
		ExitPoint = GetRandomGridPoint(WallsGrid);
	}

	ULabyrinthPassage* Exit = NewObject<ULabyrinthPassage>(WallsGrid);
	Exit->SetPassageType(ELabyrinthPassageType::Exit);
	Exit->SetGridPosition(ExitPoint);

	TArray<ULabyrinthPassage*> Passages;
	Passages.Add(Entrance);
	Passages.Add(Exit);

	return Passages;
}

FIntPoint URandomEntranceAndExitSelector::GetRandomGridPoint(UWallsGrid* WallsGrid) const
{
	const FIntPoint MaxPosition = WallsGrid->GetGridSize();
	UWallsCell* Cell;
	FIntPoint RandomPosition;

	do
	{
		RandomPosition = FIntPoint(	RandomPosition.X = (FMath::RandRange(0, MaxPosition.X)),
									RandomPosition.Y = FMath::RandRange(0, MaxPosition.Y));
		Cell = WallsGrid->GetCellAtGridPosition(RandomPosition);
	}
	while (Cell == nullptr || !Cell->IsUsed());

	return RandomPosition;
}
