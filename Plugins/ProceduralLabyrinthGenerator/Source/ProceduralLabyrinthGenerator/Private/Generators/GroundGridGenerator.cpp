#include "Generators/GroundGridGenerator.h"
#include "ToolContextInterfaces.h"

UGroundGridGenerator::UGroundGridGenerator()
{
}

UGroundGrid* UGroundGridGenerator::GenerateGroundGrid(TArray<FVector> Area, const float CellSize, UObject* Outer)
{
	//Evaluating Grid size and position
	float MinWidth = 0;
	float MinHeight = 0;
	float MaxWidth = 0;
	float MaxHeight = 0;

	for(auto& Point : Area)
	{
		if (Point.X < MinWidth)
		{
			MinWidth = Point.X;
		}
		if (Point.X > MaxWidth)
		{
			MaxWidth = Point.X;
		}
		if(Point.Y < MinHeight)
		{
			MinHeight = Point.Y;
		}
		if(Point.Y > MaxHeight)
		{
			MaxHeight = Point.Y;
		}
	}

	//Creating the grid objects with the correct size and position
	UGroundGrid* Grid = NewObject<UGroundGrid>(Outer);
	const FVector StartPosition (MinWidth, MinHeight, 0.f);
	const FVector WorldSize (FMath::Abs(MaxWidth - MinWidth), FMath::Abs(MaxHeight - MinHeight), 0.f);
	const FIntPoint GridSize (FMath::RoundToInt(WorldSize.X / CellSize), FMath::RoundToInt(WorldSize.Y / CellSize));	
	Grid->SetStartPosition(StartPosition);
	Grid->SetWorldSize(WorldSize);
	Grid->SetGridSize(GridSize);
	Grid->SetCellSize(CellSize);

	//Creating the grid cells with the correct size and position
	//Check if cell has a ground inside area
	for(int i = 0; i < GridSize.X; i ++)
	{
		for(int j = 0; j < GridSize.Y; j ++)
		{
			UGroundCell* Cell = NewObject<UGroundCell>(Grid);
			Cell->GridPosition = FVector(i, j, 0);
			Cell->WorldPosition.X = StartPosition.X + (Cell->GridPosition.X * CellSize + CellSize / 2);
			Cell->WorldPosition.Y = StartPosition.Y + (Cell->GridPosition.Y * CellSize + CellSize / 2);

			Cell->GroundPoints = GetCellGroundInformation(Area, Cell, CellSize);
			if (Cell->IsFullyGrounded())
			{
				Cell->GroundState = EGroundCellState::FullGround;
			}
			else
			{
				if (Cell->HasAnyGround())
				{
					Cell->GroundState = EGroundCellState::PartialGround;
				}
				else
				{
					Cell->GroundState = EGroundCellState::NoGround;
				}
			}

			Grid->AddCell(Cell);
		}
	}

	return Grid;
}

TArray<bool> UGroundGridGenerator::GetCellGroundInformation(TArray<FVector> PolygonVertices, UGroundCell* Cell, float CellSize)
{
	//Ground Points

	/*
	*	|6|7|8|
	*	|3|4|5|
	*	|0|1|2|
	*/

	TArray<FVector> Positions;
	Positions.Add(FVector(Cell->WorldPosition.X - CellSize / 2,	Cell->WorldPosition.Y - CellSize / 2, 0.f));
	Positions.Add(FVector(Cell->WorldPosition.X,					Cell->WorldPosition.Y - CellSize / 2, 0.f));
	Positions.Add(FVector(Cell->WorldPosition.X + CellSize / 2,	Cell->WorldPosition.Y - CellSize / 2, 0.f));

	Positions.Add(FVector(Cell->WorldPosition.X - CellSize / 2,	Cell->WorldPosition.Y, 0.f));
	Positions.Add(FVector(Cell->WorldPosition.X, 				    Cell->WorldPosition.Y, 0.f));
	Positions.Add(FVector(Cell->WorldPosition.X + CellSize / 2, 	Cell->WorldPosition.Y, 0.f));

	Positions.Add(FVector(Cell->WorldPosition.X - CellSize / 2,	Cell->WorldPosition.Y + CellSize / 2, 0.f));       
	Positions.Add(FVector(Cell->WorldPosition.X,					Cell->WorldPosition.Y + CellSize / 2, 0.f));   
	Positions.Add(FVector(Cell->WorldPosition.X + CellSize / 2,	Cell->WorldPosition.Y + CellSize / 2, 0.f));       

	TArray<bool> IsPositionGround;
	IsPositionGround.SetNum(9);

	for(int i = 0; i < IsPositionGround.Num(); i ++)
	{
		IsPositionGround[i] = IsPositionInsideArea(PolygonVertices, Positions[i]);
	}

	return IsPositionGround;
}

bool UGroundGridGenerator::IsPositionInsideArea(TArray<FVector> Area, const FVector Position)
{
	int TotalIntersections = 0;
	const FVector PositionSegmentEnd = Position + FVector::ForwardVector * 1000000.f;

	for(int i = 0; i < Area.Num(); i ++)
	{
		FVector SegmentAStart = Area[i];
		FVector SegmentAEnd = Area[i + 1 >= Area.Num() ? 0 : i +1];
		FVector IntersectionPoint;

		if(FMath::SegmentIntersection2D(SegmentAStart, SegmentAEnd, Position,
			PositionSegmentEnd, IntersectionPoint))
		{
			TotalIntersections ++;
		}
	}

	return TotalIntersections % 2 != 0;
}
