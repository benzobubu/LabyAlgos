#include "Utilities/LabyrinthUtilities.h"
#include "Cells/RecursiveBacktrackingDirectionEnum.h"
#include "Graph/LabyrinthGraph.h"


ULabyrinthGraph* ULabyrinthUtilities::GetGraphFromCell(UWallsGrid* Grid, UWallsCell* StartCell)
{
	ULabyrinthGraph* Graph = NewObject<ULabyrinthGraph>(Grid);
	TArray<UWallsCell*> ToCheck;
	ToCheck.Add(StartCell);

	while(ToCheck.Num() > 0)
	{
		UWallsCell* Cell = ToCheck[0];
		ULabyrinthVertex* Vertex;
		if (!Graph->VertexCellExists(Cell))
		{
			Vertex = NewObject<ULabyrinthVertex>();
			Vertex->SetCell(Cell);
			Graph->AddVertex(Vertex);
		}
		else
		{
			Vertex = Graph->GetCellVertex(Cell);
		}

		TArray<UWallsCell*> ConnectedCells = GetConnectedNeighborCells(Grid, Cell);
		for (UWallsCell* ConnectedCell : ConnectedCells)
		{
			if(!Graph->VertexCellExists(ConnectedCell))
			{
				ULabyrinthVertex* ConnectedVertex = NewObject<ULabyrinthVertex>();
				ConnectedVertex->SetCell(ConnectedCell);
				Vertex->Adjacents.Add(ConnectedVertex);
				Graph->AddVertex(ConnectedVertex);
				ToCheck.Add(ConnectedCell);
			}
			else
			{
				ULabyrinthVertex* ConnectedVertex = Graph->GetCellVertex(ConnectedCell);
				Vertex->Adjacents.Add(ConnectedVertex);
			}
		}

		ToCheck.RemoveAt(0);
	}

	return Graph;
}

void ULabyrinthUtilities::RemoveAllWallsFromSamePathCells(UWallsGrid* Grid)
{
	TArray<ULabyrinthGraph*> Graphs = ULabyrinthUtilities::GetGraphsFromGrid(Grid);
	for (int i = 0; i < Graphs.Num(); i ++)
	{
		ULabyrinthGraph* Graph = Graphs[i];
		RemoveDeadEndInteriorWallsFromGraph(Grid, Graph);
	}
}

TArray<ULabyrinthGraph*> ULabyrinthUtilities::GetGraphsFromGrid(UWallsGrid* Grid)
{
	TArray<ULabyrinthGraph*> Graphs;
	const FIntPoint GridSize = Grid->GetGridSize();

	for (int i = 0; i < GridSize.X; i ++)
	{
		for (int j = 0; j < GridSize.Y; j ++)
		{
			UWallsCell* CurrentCell = Grid->GetCellAtGridPosition(FIntPoint(i, j));
			if (CurrentCell != nullptr && CurrentCell->IsUsed())
			{
				//Check if the cell is already included in a graph
				bool IsAlreadyInGraph = false;
				for (ULabyrinthGraph* Graph : Graphs)
				{
					if (Graph->VertexCellExists(CurrentCell))
					{
						IsAlreadyInGraph = true;
						break;
					}
				}

				//Generate new graph starting from this cell
				if (!IsAlreadyInGraph)
				{
					ULabyrinthGraph* Graph = GetGraphFromCell(Grid, CurrentCell);
					if (Graph != nullptr)
					{
						Graphs.Add(Graph);
					}
				}
			}
		}
	}
	return Graphs;
}

TArray<ULabyrinthGraph*> ULabyrinthUtilities::SeparateGraphsAtNodes(ULabyrinthGraph* Graph)
{
	TArray<ULabyrinthVertex*> Nodes = GetNodes(Graph);
	ULabyrinthGraph* TempGraph = DuplicateObject(Graph, Graph);
	for (ULabyrinthVertex* Node : Nodes)
	{
		TempGraph->Vertices.Remove(Node);
		for (ULabyrinthVertex* Vertex : TempGraph->Vertices)
		{
			Vertex->Adjacents.Remove(Node);
		}
	}

	TArray<ULabyrinthGraph*> SeparatedGraphs;
	while (TempGraph->Vertices.Num() > 0)
	{
		ULabyrinthVertex* StartVertex = nullptr;
		for (ULabyrinthVertex* Vertex : TempGraph->Vertices)
		{
			if (Vertex->Adjacents.Num() <= 1)
			{
				StartVertex = Vertex;
				break;
			}
		}

		ULabyrinthGraph* NewGraph = NewObject<ULabyrinthGraph>();
		if (StartVertex != nullptr)
		{
			if (StartVertex->Adjacents.Num() == 0)
			{
				TempGraph->Vertices.Remove(StartVertex);
				NewGraph->Vertices.Add(StartVertex);
			}
			else if (StartVertex->Adjacents.Num() == 1)
			{
				auto Vert1 = StartVertex;
				auto Vert2 = Vert1->Adjacents[0];

				while (Vert2->Adjacents.Num() == 2)
				{
					TempGraph->Vertices.Remove(Vert1);
					NewGraph->Vertices.Add(Vert1);

					ULabyrinthVertex* Next;
					if (Vert2->Adjacents[0] == Vert1)
					{
						Next = Vert2->Adjacents[1];
					}
					else
					{
						Next = Vert2->Adjacents[0];
					}

					Vert1 = Vert2;
					Vert2 = Next;
				}

				TempGraph->Vertices.Remove(Vert1);
				TempGraph->Vertices.Remove(Vert2);
				NewGraph->Vertices.Add(Vert1);
				NewGraph->Vertices.Add(Vert2);
			}

			SeparatedGraphs.Add(NewGraph);
		}
	}

	return SeparatedGraphs;
}

TArray<ULabyrinthVertex*> ULabyrinthUtilities::GetNodes(ULabyrinthGraph* Graph)
{
	TArray<ULabyrinthVertex*> Vertices;
	for(ULabyrinthVertex* Vertex : Graph->Vertices)
	{
		if (Vertex->Adjacents.Num() > 2)
		{
			Vertices.Add(Vertex);
		}
	}

	return Vertices;
}

TArray<UWallsCell*> ULabyrinthUtilities::GetConnectedNeighborCells(UWallsGrid* Grid, UWallsCell* Cell)
{
	TArray<UWallsCell*> Cells;
	const FIntPoint CurrentPosition = Grid->GetCellPosition(Cell);

	for (int i = 0; i < Cell->GetWalls().Num(); i++)
	{
		if (!Cell->GetWalls()[i])
		{
			const FIntPoint NeighborPosition = CurrentPosition + GetPointFromLabyrinthDirection(i);
			if (NeighborPosition.X >= 0
				&& NeighborPosition.Y >= 0
				&& NeighborPosition.X < Grid->GetGridSize().X
				&& NeighborPosition.Y < Grid->GetGridSize().Y)
			{
				UWallsCell* Neighbor = Grid->GetCellAtGridPosition(CurrentPosition + GetPointFromLabyrinthDirection(i));
				if (Neighbor != nullptr)
				{
					Cells.Add(Neighbor);
				}
			}
		}
	}

	return Cells;
}

TArray<UWallsCell*> ULabyrinthUtilities::GetSeparatedNeighborCells(UWallsGrid* Grid,
	UWallsCell* Cell)
{
	TArray<UWallsCell*> Cells;
	const FIntPoint CurrentPosition = Grid->GetCellPosition(Cell);

	for (int i = 0; i < Cell->GetWalls().Num(); i++)
	{
		if (Cell->GetWalls()[i])
		{
			const FIntPoint NeighborPosition = CurrentPosition + GetPointFromLabyrinthDirection(i);
			if (NeighborPosition.X >= 0
				&& NeighborPosition.Y >= 0
				&& NeighborPosition.X < Grid->GetGridSize().X
				&& NeighborPosition.Y < Grid->GetGridSize().Y)
			{
				UWallsCell* Neighbor = Grid->GetCellAtGridPosition(CurrentPosition + GetPointFromLabyrinthDirection(i));
				if (Neighbor != nullptr)
				{
					Cells.Add(Neighbor);
				}
			}
		}
	}

	return Cells;
}

FIntPoint ULabyrinthUtilities::GetPointFromLabyrinthDirection(const uint8 Direction)
{
	switch (Direction)
	{
		case Top:
			return FIntPoint (0, 1);
		case Bottom:
			return FIntPoint (0, - 1);
		case Left:
			return FIntPoint (- 1, 0);
		case Right:
			return FIntPoint (1, 0);
		default:
			UE_LOG(LogTemp, Warning, TEXT("ULabyrinthUtilities::GetPointFromLabyrinthDirection, Incorrect Direction index: %d"), Direction);
			return FIntPoint::ZeroValue;
	}
}

uint8 ULabyrinthUtilities::GetDirectionFromNeighborCells(const UWallsGrid* Grid, UWallsCell* Start, UWallsCell* End)
{
	const FIntPoint Result = Grid->GetCellPosition(End) - Grid->GetCellPosition(Start);
	if (Result == FIntPoint (0, 1))
	{
		return Top;
	}
	if (Result == FIntPoint (0, - 1))
	{
		return Bottom;
	}
	if (Result ==  FIntPoint (- 1, 0))
	{
		return Left;
	}
	if (Result == FIntPoint (1, 0))
	{
		return Right;
	}
	return -1;
}


UWallsGrid* ULabyrinthUtilities::GetLabyrinthGridSection(UWallsGrid* Grid, const FIntPoint Position, const FIntPoint Size)
{
	UWallsGrid* GridSection = NewObject<UWallsGrid>();
	GridSection->SetGridSize(Size);
	GridSection->SetStartPosition(FVector(Grid->GetStartPosition().X + Position.X * Grid->GetCellSize(),
									  Grid->GetStartPosition().Y + Position.Y * Grid->GetCellSize(),
									  0.f));
	GridSection->SetCellSize(Grid->GetCellSize());
	GridSection->SetWorldSize(Grid->GetWorldSize());

	for(int i = 0; i < Size.X; i ++)
	{
		for(int j = 0; j < Size.Y; j ++)
		{
			const UWallsCell* Original = Grid->GetCellAtGridPosition(FIntPoint(Position.X + i, Position.Y + j));
			UWallsCell* Copy = DuplicateObject(Original, GridSection);
			GridSection->AddCell(Copy);
		}
	}

	return GridSection;
}

void ULabyrinthUtilities::RemoveWallsFromGridSection(UWallsGrid* Grid, const FIntPoint Position, const FIntPoint Size)
{
	for(int i = 0; i < Size.X; i ++)
	{
		for(int j = 0; j < Size.Y; j ++)
		{
			UWallsCell* Current = Grid->GetCellAtGridPosition(FIntPoint(Position.X + i, Position.Y + j));
			if (Current != nullptr)
			{
				Current->SetWalls(TArray<bool>{false, false, false, false});

				if(i == 0)
				{
					UWallsCell* Neighbor = Grid->GetCellAtGridPosition(FIntPoint(Position.X - 1, Position.Y + j));
					if (Neighbor != nullptr)
					{
						Neighbor->SetWall(Right, false);
					}
				}
				else if (i == Size.X -1)
				{
					UWallsCell* Neighbor = Grid->GetCellAtGridPosition(FIntPoint(Position.X + i + 1, Position.Y + j));
					if (Neighbor != nullptr)
					{
						Neighbor->SetWall(Left, false);
					}
				}

				if(j == 0)
				{
					UWallsCell* Neighbor = Grid->GetCellAtGridPosition(FIntPoint(Position.X + i, Position.Y - 1));
					if (Neighbor != nullptr)
					{
						Neighbor->SetWall(Top, false);
					}
				}
				else if(j == Size.Y - 1)
				{
					UWallsCell* Neighbor = Grid->GetCellAtGridPosition(FIntPoint(Position.X + i, Position.Y + j + 1));
					if (Neighbor != nullptr)
					{
						Neighbor->SetWall(Bottom, false);
					}
				}
			}
		}
	}
}

bool ULabyrinthUtilities::IsGridSectionFullyUsed(UWallsGrid* Grid, const FIntPoint Position, const FIntPoint Size)
{
	for(int i = 0; i < Size.X; i ++)
	{
		for(int j = 0; j < Size.Y; j ++)
		{
			const UWallsCell* Cell = Grid->GetCellAtGridPosition(FIntPoint(Position.X + i, Position.Y + j));
			if (!Cell->IsUsed())
			{
				return false;
			}
		}
	}
	return true;
}

TArray<FIntPoint> ULabyrinthUtilities::GetExternalOutlineCells(UWallsGrid* Grid)
{
	const FIntPoint GridSize = Grid->GetGridSize();
	TArray<FIntPoint> OutlineCells;

	for(int i = 0; i < GridSize.X; i ++)
	{
		for(int j = 0; j < GridSize.Y; j ++)
		{
			FIntPoint CurrentCellPos (i, j);
			UWallsCell* Cell = Grid->GetCellAtGridPosition(CurrentCellPos);

			if(Cell != nullptr && Cell->IsUsed())
			{
				if(CurrentCellPos.X == 0 || CurrentCellPos.Y == 0 || CurrentCellPos.X == GridSize.X - 1 || CurrentCellPos.Y == GridSize.Y - 1)
				{
					OutlineCells.Add(CurrentCellPos);
				}
				else
				{
					if(!Grid->GetCellAtGridPosition(CurrentCellPos + FIntPoint(1, 0))->IsUsed()
						|| !Grid->GetCellAtGridPosition(CurrentCellPos + FIntPoint(- 1, 0))->IsUsed()
						|| !Grid->GetCellAtGridPosition(CurrentCellPos + FIntPoint(0, 1))->IsUsed()
						|| !Grid->GetCellAtGridPosition(CurrentCellPos + FIntPoint(0, - 1))->IsUsed())
					{
						OutlineCells.Add(CurrentCellPos);
					}
				}
			}
		}
	}

	return OutlineCells;
}

TArray<bool> ULabyrinthUtilities::GetCellExternalOpenings(UWallsGrid* Grid, FIntPoint Position)
{
	TArray<bool> Sides = GetCellExternalOutlineSides(Grid, Position);
	if(Sides.Num() != 4)
	{
		UE_LOG(LogTemp, Warning, TEXT("ULabyrinthUtilities::GetCellExternalOpennings error: Incorrect TArray<bool> length"))
		return TArray<bool>{false,false,false,false};
	}

	UWallsCell* Cell = Grid->GetCellAtGridPosition(Position);
	if(Cell != nullptr)
	{
		TArray<bool> Walls = Cell->GetWalls();
		return TArray<bool> {
			Sides[0] && !Walls[0],
			Sides[1] && !Walls[1],
			Sides[2] && !Walls[2],
			Sides[3] && !Walls[3]
		};
	}

	return TArray<bool>{false,false,false,false};
}

TArray<bool> ULabyrinthUtilities::GetCellExternalWalls(UWallsGrid* Grid, FIntPoint Position)
{
	TArray<bool> Sides = GetCellExternalOutlineSides(Grid, Position);
	if(Sides.Num() != 4)
	{
		UE_LOG(LogTemp, Warning, TEXT("ULabyrinthUtilities::GetCellExternalWalls error: Incorrect TArray<bool> length"))
		return TArray<bool>{false,false,false,false};
	}

	UWallsCell* Cell = Grid->GetCellAtGridPosition(Position);
	if(Cell != nullptr)
	{
		TArray<bool> Walls = Cell->GetWalls();
		return TArray<bool> {
			Sides[0] && Walls[0],
			Sides[1] && Walls[1],
			Sides[2] && Walls[2],
			Sides[3] && Walls[3]
		};
	}

	return TArray<bool>{false,false,false,false};
}

TArray<bool> ULabyrinthUtilities::GetCellExternalOutlineSides(UWallsGrid* Grid, const FIntPoint Position)
{
	const FIntPoint GridSize = Grid->GetGridSize();
	TArray<bool> ExternalSides {false, false, false, false};

	if(Position.X == 0)
	{
		ExternalSides[Left] = true;
	}
	else
	{
		UWallsCell* Neighbor = Grid->GetCellAtGridPosition(Position + FIntPoint(- 1, 0));
		if(Neighbor != nullptr && !Neighbor->IsUsed())
		{
			ExternalSides[Left] = true;
		}
	}

	if(Position.X == GridSize.X - 1)
	{
		ExternalSides[Right] = true;
	}
	else
	{
		UWallsCell* Neighbor = Grid->GetCellAtGridPosition(Position + FIntPoint(1, 0));
		if(Neighbor != nullptr && !Neighbor->IsUsed())
		{
			ExternalSides[Right] = true;
		}
	}

	if(Position.Y == 0)
	{
		ExternalSides[Bottom] = true;
	}
	else
	{
		UWallsCell* Neighbor = Grid->GetCellAtGridPosition(Position + FIntPoint(0, - 1));
		if(Neighbor != nullptr && !Neighbor->IsUsed())
		{
			ExternalSides[Bottom] = true;
		}
	}

	if(Position.Y == GridSize.Y - 1)
	{
		ExternalSides[Top] = true;
	}
	else
	{
		UWallsCell* Neighbor = Grid->GetCellAtGridPosition(Position + FIntPoint(0, 1));
		if(Neighbor != nullptr && !Neighbor->IsUsed())
		{
			ExternalSides[Top] = true;
		}
	}

	return ExternalSides;
}

void ULabyrinthUtilities::RemoveDeadEndInteriorWalls(UWallsGrid* LabyrinthGrid)
{
	TArray<ULabyrinthGraph*> Graphs = GetGraphsFromGrid(LabyrinthGrid);
	TArray<ULabyrinthGraph*> SeparatedGraphs = SeparateGraphsAtNodes(Graphs[0]);
	for (ULabyrinthGraph* Graph : SeparatedGraphs)
	{
		RemoveDeadEndInteriorWallsFromGraph(LabyrinthGrid, Graph);
	}
}

void ULabyrinthUtilities::RemoveDeadEndInteriorWallsFromGraph(UWallsGrid* Grid, ULabyrinthGraph* Graph)
{
	for (int i = 0; i < Graph->Vertices.Num() - 1; i ++)
	{
		UWallsCell* Current = Graph->Vertices[i]->GetCell();
		TArray<UWallsCell*> Neighbors = GetSeparatedNeighborCells(Grid, Current);
		for(UWallsCell* Neighbor : Neighbors)
		{
			if (Graph->VertexCellExists(Neighbor))
			{
				switch (GetDirectionFromNeighborCells(Grid, Current, Neighbor))
				{
					case Top:
						Current->SetWall(Top, false);
						Neighbor->SetWall(Bottom, false);
						break;
					case Bottom:
						Current->SetWall(Bottom, false);
						Neighbor->SetWall(Top, false);
						break;
					case Left:
						Current->SetWall(Left, false);
						Neighbor->SetWall(Right, false);
						break;
					case Right:
						Current->SetWall(Right, false);
						Neighbor->SetWall(Left, false);
						break;
					default:
						UE_LOG(LogTemp, Warning, TEXT("ULabyrinthUtilities::RemoveWallsFromSamePathCells, Incorrect neighbors direction"));
				}
			}
		}
	}
}
