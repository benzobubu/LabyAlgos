#include "Grids/RecursiveBacktrackingGrid.h"

void URecursiveBacktrackingGrid::SetSize(const FIntPoint NewSize)
{
	Size = NewSize;
}

FIntPoint URecursiveBacktrackingGrid::GetSize() const
{
	return Size;
}
