#include "Grids/WorldGridBase.h"

FVector UWorldGridBase::GetStartPosition() const
{
	return StartPosition;
}

void UWorldGridBase::SetStartPosition(FVector Position)
{
	StartPosition = Position;
}

FVector UWorldGridBase::GetCellWorldPosition(FIntPoint GridPosition) const
{
	return StartPosition + FVector(1,1,0) * CellSize/2 + FVector(GridPosition.X * CellSize, GridPosition.Y * CellSize, 0.f);
}

float UWorldGridBase::GetCellSize() const
{
	return CellSize;
}

void UWorldGridBase::SetCellSize(float Size)
{
	CellSize = Size;
}

void UWorldGridBase::SetWorldSize(FVector Size)
{
	WorldSize = Size;
}

FVector UWorldGridBase::GetWorldSize()
{
	return WorldSize;
}
