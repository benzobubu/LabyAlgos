#include "Grids/GridBase.h"

void UGridBase::SetGridSize(FIntPoint NewSize)
{
	GridSize = NewSize;
}

FIntPoint UGridBase::GetGridSize() const
{
	return GridSize;
}

UCellBase* UGridBase::GetCellAtGridPosition(const FIntPoint Position)
{
	const int Index = GetCellIndexAtPosition(Position);

	if (Index < Cells.Num())
	{
		return Cells[Index];
	}

	return nullptr;
}

void UGridBase::AddCell(UCellBase* Cell)
{
	Cells.Add(Cell);
}

UCellBase* UGridBase::GetCellAtIndex(const int Index)
{
	if (Index < Cells.Num())
	{
		return Cells[Index];
	}

	return nullptr;
}

int UGridBase::GetCellsNum() const
{
	return Cells.Num();
}

int UGridBase::GetCellIndexAtPosition(const FIntPoint Position) const
{
	return Position.X * GridSize.Y + Position.Y;
}

FIntPoint UGridBase::GetCellPositionAtIndex(const int Index) const
{
	const int X = Index / GridSize.Y;
	const int Y = Index % GridSize.Y;
	return FIntPoint(X, Y);
}
