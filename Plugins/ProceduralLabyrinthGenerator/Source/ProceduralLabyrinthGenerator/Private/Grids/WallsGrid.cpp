#include "Grids/WallsGrid.h"
#include "Cells/WallsCell.h"

UWallsCell* UWallsGrid::GetCellAtGridPosition(const FIntPoint Position)
{
	return Cast<UWallsCell>(Super::GetCellAtGridPosition(Position));
}

FIntPoint UWallsGrid::GetCellPosition(UWallsCell* Cell) const
{
	const int Index = Cells.Find(Cell);
	return GetCellPositionAtIndex(Index);
}
