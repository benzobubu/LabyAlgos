#include "Grids/GroundGrid.h"
#include "Cells/GroundCell.h"

UGroundCell* UGroundGrid::GetCellAtIndex(int Index)
{
	return Cast<UGroundCell>(Super::GetCellAtIndex(Index));
}

UGroundCell* UGroundGrid::GetCellAtGridPosition(FIntPoint Position)
{
	return Cast<UGroundCell>(Super::GetCellAtGridPosition(Position));
}
