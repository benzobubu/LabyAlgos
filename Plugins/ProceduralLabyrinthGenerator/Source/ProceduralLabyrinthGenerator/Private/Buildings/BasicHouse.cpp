#include "Buildings/BasicHouse.h"

#include "Cells/RecursiveBacktrackingDirectionEnum.h"
#include "Utilities/LabyrinthUtilities.h"

ABasicHouse::ABasicHouse()
{
	FloorInstancedStaticMesheComp = CreateDefaultSubobject<UInstancedStaticMeshComponent>(TEXT("Floors"));
	FloorInstancedStaticMesheComp->SetupAttachment(RootComponent);

	RoofInstancedStaticMeshComp = CreateDefaultSubobject<UInstancedStaticMeshComponent>(TEXT("Roofs"));
	RoofInstancedStaticMeshComp->SetupAttachment(RootComponent);

	WallInstancedStaticMeshComp = CreateDefaultSubobject<UInstancedStaticMeshComponent>(TEXT("Walls"));
	WallInstancedStaticMeshComp->SetupAttachment(RootComponent);

	DoorInstancedStaticMeshComp = CreateDefaultSubobject<UInstancedStaticMeshComponent>(TEXT("Doors"));
	DoorInstancedStaticMeshComp->SetupAttachment(RootComponent);
}

void ABasicHouse::Build()
{
	if (LabyrinthGrid != nullptr)
	{
		ULabyrinthUtilities::RemoveAllWallsFromSamePathCells(LabyrinthGrid);
		BuildFloor();
		BuildWalls();
		BuildDoors();
		BuildRoof();
	}
}

void ABasicHouse::SetWallsGrid(UWallsGrid* Grid)
{
	if (Grid != nullptr)
	{
		LabyrinthGrid = Grid;
	}
}

void ABasicHouse::BuildFloor() const
{
	if(LabyrinthGrid != nullptr && FloorMesh != nullptr)
	{
		FloorInstancedStaticMesheComp->SetStaticMesh(FloorMesh);
		for(int i = 0; i < LabyrinthGrid->GetGridSize().X; i ++)
		{
			for(int j = 0; j < LabyrinthGrid->GetGridSize().Y; j ++)
			{
				const FIntPoint GridPosition(i, j);
				const FVector WorldPosition = LabyrinthGrid->GetCellWorldPosition(GridPosition);
				const FTransform InstanceTransform (FRotator(0.f, 0.f, 0.f),
													WorldPosition,
													FVector(1, 1, 1));
				FloorInstancedStaticMesheComp->AddInstance(InstanceTransform);
			}
		}
	}
}

void ABasicHouse::BuildWalls() const
{
	if(LabyrinthGrid != nullptr && WallMesh != nullptr)
	{
		WallInstancedStaticMeshComp->SetStaticMesh(WallMesh);
		float Offset = CellSize / 2;
		for(int i = 0; i < LabyrinthGrid->GetGridSize().X; i ++)
		{
			for(int j = 0; j < LabyrinthGrid->GetGridSize().Y; j ++)
			{
				const FIntPoint GridPosition(i, j);
				const UWallsCell* Cell = LabyrinthGrid->GetCellAtGridPosition(GridPosition);
				const FVector WorldPosition = LabyrinthGrid->GetCellWorldPosition(GridPosition);
				if(Cell != nullptr)
				{
					if(Cell->GetWalls()[Bottom])
					{
						const FTransform InstanceTransform (FRotator(0.f, 0.f, 0.f),
															WorldPosition + FVector(0, - Offset, 0.f),
															FVector::OneVector);
						WallInstancedStaticMeshComp->AddInstance(InstanceTransform);
					}
					if(Cell->GetWalls()[Right])
					{
						const FTransform InstanceTransform (FRotator(0.f, 90.f, 0.f),
															WorldPosition + FVector(Offset, 0.f, 0.f),
															FVector::OneVector);
						WallInstancedStaticMeshComp->AddInstance(InstanceTransform);
					}
					if(Cell->GetWalls()[Top])
					{
						const FTransform InstanceTransform (FRotator(0.f, 180.f, 0.f),
															WorldPosition + FVector(0.f, Offset, 0.f),
															FVector::OneVector);
						WallInstancedStaticMeshComp->AddInstance(InstanceTransform);
					}
					if(Cell->GetWalls()[Left])
					{
						const FTransform InstanceTransform (FRotator(0.f, -90.f, 0.f),
															WorldPosition + FVector(- Offset, 0.f, 0.f),
															FVector::OneVector);
						WallInstancedStaticMeshComp->AddInstance(InstanceTransform);
					}
				}
			}
		}
	}
}

void ABasicHouse::BuildRoof() const
{
	if(LabyrinthGrid != nullptr && RoofMesh != nullptr)
	{
		RoofInstancedStaticMeshComp->SetStaticMesh(RoofMesh);
		for(int i = 0; i < LabyrinthGrid->GetGridSize().X; i ++)
		{
			for(int j = 0; j < LabyrinthGrid->GetGridSize().Y; j ++)
			{
				const FIntPoint GridPosition(i, j);
				const FVector WorldPosition = LabyrinthGrid->GetCellWorldPosition(GridPosition) + FVector::UpVector * RoofHeight;
				const FTransform InstanceTransform (FRotator(0.f, 0.f, 0.f),
													WorldPosition,
													FVector(1, 1, 1));
				RoofInstancedStaticMeshComp->AddInstance(InstanceTransform);
			}
		}
	}
}

void ABasicHouse::BuildDoors() const
{
	if(LabyrinthGrid != nullptr && Door != nullptr)
	{
		DoorInstancedStaticMeshComp->SetStaticMesh(Door);
		const float Offset = CellSize / 2;
		TArray<FIntPoint> Pos = ULabyrinthUtilities::GetExternalOutlineCells(LabyrinthGrid);
		for(FIntPoint Position : Pos)
		{
			FVector WorldPosition = LabyrinthGrid->GetCellWorldPosition(Position);
			TArray<bool> Sides = ULabyrinthUtilities::GetCellExternalOpenings(LabyrinthGrid, Position);
			if(Sides[Left])
			{
				const FTransform InstanceTransform (FRotator(0.f, -90.f, 0.f),
													WorldPosition + FVector(- Offset,  0.f, 0.f),
													FVector::OneVector);
				DoorInstancedStaticMeshComp->AddInstance(InstanceTransform);
			}
			if(Sides[Right])
			{
				const FTransform InstanceTransform (FRotator(0.f, 90.f, 0.f),
													WorldPosition + FVector(Offset,0.f, 0.f),
													FVector::OneVector);
				DoorInstancedStaticMeshComp->AddInstance(InstanceTransform);
			}
			if(Sides[Top])
			{
				const FTransform InstanceTransform (FRotator(0.f, 180.f, 0.f),
													WorldPosition + FVector::RightVector * Offset,
													FVector::OneVector);
				DoorInstancedStaticMeshComp->AddInstance(InstanceTransform);
			}
			if(Sides[Bottom])
			{
				const FTransform InstanceTransform (FRotator(0.f, 0.f, 0.f),
													WorldPosition + FVector::RightVector * - Offset,
													FVector::OneVector);
				DoorInstancedStaticMeshComp->AddInstance(InstanceTransform);
			}
		}
	}
}

void ABasicHouse::BuildExternalWalls() const
{
	if(LabyrinthGrid != nullptr && WallInstancedStaticMeshComp != nullptr)
	{
		WallInstancedStaticMeshComp->SetStaticMesh(WallMesh);
		float Offset = CellSize / 2;
		TArray<FIntPoint> Pos = ULabyrinthUtilities::GetExternalOutlineCells(LabyrinthGrid);
		for(FIntPoint Position : Pos)
		{
			FVector WorldPosition = LabyrinthGrid->GetCellWorldPosition(Position);
			TArray<bool> CellWalls = ULabyrinthUtilities::GetCellExternalWalls(LabyrinthGrid, Position);
			if(CellWalls[Left])
			{
				const FTransform InstanceTransform (FRotator(0.f, -90.f, 0.f),
													WorldPosition + FVector(- Offset,  0.f, 0.f),
													FVector::OneVector);
				WallInstancedStaticMeshComp->AddInstance(InstanceTransform);
			}
			if(CellWalls[Right])
			{
				const FTransform InstanceTransform (FRotator(0.f, 90.f, 0.f),
													WorldPosition + FVector(Offset,
													0.f, 0.f),FVector::OneVector);
				WallInstancedStaticMeshComp->AddInstance(InstanceTransform);
			}
			if(CellWalls[Top])
			{
				const FTransform InstanceTransform (FRotator(0.f, 180.f, 0.f),
													WorldPosition + FVector::RightVector * Offset,
													FVector::OneVector);
				WallInstancedStaticMeshComp->AddInstance(InstanceTransform);
			}
			if(CellWalls[Bottom])
			{
				const FTransform InstanceTransform (FRotator(0.f, 0.f, 0.f),
													WorldPosition + FVector::RightVector * - Offset,
													FVector::OneVector);
				WallInstancedStaticMeshComp->AddInstance(InstanceTransform);
			}
		}
	}
}
