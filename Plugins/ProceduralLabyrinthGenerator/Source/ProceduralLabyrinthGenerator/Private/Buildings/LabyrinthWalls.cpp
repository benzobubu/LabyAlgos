#include "Buildings/LabyrinthWalls.h"
#include "Cells/RecursiveBacktrackingDirectionEnum.h"

ALabyrinthWalls::ALabyrinthWalls()
{
	Walls = CreateDefaultSubobject<UInstancedStaticMeshComponent>(TEXT("SimpleWalls"));
	Walls->SetupAttachment(RootComponent);
}

void ALabyrinthWalls::Build()
{
	if (LabyrinthGrid != nullptr)
	{
		SpawnWalls(LabyrinthGrid);
	}
}

void ALabyrinthWalls::SetLabyrinthGrid(UWallsGrid* Grid)
{
	LabyrinthGrid = Grid;
}

void ALabyrinthWalls::SpawnWalls(UWallsGrid* Grid) const
{
	if(Grid != nullptr && WallMesh != nullptr)
	{
		Walls->SetStaticMesh(WallMesh);
		float CellSize = Grid->GetCellSize();
		float Offset = CellSize / 2;
		for(int i = 0; i < Grid->GetGridSize().X; i ++)
		{
			for(int j = 0; j < Grid->GetGridSize().Y; j ++)
			{
				FIntPoint GridPosition(i, j);
				UWallsCell* Cell = Grid->GetCellAtGridPosition(GridPosition);
				FVector WorldPosition = Grid->GetCellWorldPosition(GridPosition);
				if(Cell->HasAnyWall())
				{
					TArray<bool> CellWalls = Cell->GetWalls();
					if(CellWalls[Bottom])
					{
						const FTransform InstanceTransform (
							FRotator(0.f, 0.f, 0.f),
							WorldPosition + FVector(0, - Offset, 0.f),
							FVector::OneVector);
						Walls->AddInstance(InstanceTransform);
					}
					if(CellWalls[Right])
					{
						const FTransform InstanceTransform (
							FRotator(0.f, 90.f, 0.f),
							WorldPosition + FVector(Offset, 0.f, 0.f),
							FVector::OneVector);
						Walls->AddInstance(InstanceTransform);
					}
					if(CellWalls[Top])
					{
						const FTransform InstanceTransform (
							FRotator(0.f, 180.f, 0.f),
							WorldPosition + FVector(0.f, Offset, 0.f),
							FVector::OneVector);
						Walls->AddInstance(InstanceTransform);
					}
					if(CellWalls[Left])
					{
						const FTransform InstanceTransform (
							FRotator(0.f, -90.f, 0.f),
							WorldPosition + FVector(- Offset, 0.f, 0.f),
							FVector::OneVector);
						Walls->AddInstance(InstanceTransform);
					}
				}
			}
		}
	}
}
