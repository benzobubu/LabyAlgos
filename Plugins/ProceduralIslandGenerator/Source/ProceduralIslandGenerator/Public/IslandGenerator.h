#pragma once

#include "CoreMinimal.h"
#include "IslandMeshGenerator.h"
#include "IslandProceduralMesh.h"
#include "GameFramework/Actor.h"
#include "IslandGenerator.generated.h"

UCLASS()
class PROCEDURALISLANDGENERATOR_API AIslandGenerator : public AActor
{
	GENERATED_BODY()

public:
	AIslandGenerator();

	UFUNCTION(BlueprintCallable, CallInEditor, Category="Generation")
	void GenerateIsland();

	UFUNCTION(BlueprintCallable, CallInEditor, Category="Generation")
	void RemoveCurrentIsland();

	UFUNCTION(BlueprintCallable, Category="Seed")
	void SetSeed(int32 NewSeed);

	UFUNCTION(BlueprintCallable, Category="Size")
	void SetIslandMaxSize(float Size);

	UFUNCTION(BlueprintCallable, Category="Size")
	void SetIslandMinSize(float Size);

	UFUNCTION(BlueprintCallable, Category="Floor")
	TArray<FVector>& GetIslandFloor();

protected:
	UPROPERTY(EditAnywhere, Category="Generation")
	TSubclassOf<UIslandMeshGenerator> IslandMeshGeneratorClass;

	UPROPERTY(EditInstanceOnly, Category="Generation")
	UIslandMeshGenerator* IslandMeshGenerator;

	UPROPERTY(VisibleInstanceOnly, Category="Island")
	AIslandProceduralMesh* IslandMesh;

	UPROPERTY(EditAnywhere, Category="Generation")
	int32 Seed;

	UPROPERTY(EditAnywhere, Category="Shape")
	float MinimumSize = 1000.f;

	UPROPERTY(EditAnywhere, Category="Shape")
	float MaximumSize = 2000.f;

	UPROPERTY(EditAnywhere, Category="Shape")
	int ShapeVerticesCount = 25;

	UPROPERTY(VisibleInstanceOnly, Category="Shape")
	TArray<FVector> Vertices;

};
