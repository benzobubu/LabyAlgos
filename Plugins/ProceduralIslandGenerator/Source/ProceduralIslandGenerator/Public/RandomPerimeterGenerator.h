#pragma once

#include "CoreMinimal.h"

class PROCEDURALISLANDGENERATOR_API URandomPerimeterGenerator : public UBlueprintFunctionLibrary
{
public:
	URandomPerimeterGenerator();

	static TArray<FVector> Generate(const int Seed, const int PerimeterPointsCount, const float RadiusMinimum, const float RadiusMaximum);

protected:
	static FVector GetPositionAtAngleAndDistance(FVector Position, float Angle, float Distance);

};
