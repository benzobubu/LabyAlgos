#pragma once

#include "CoreMinimal.h"
#include "IslandProceduralMesh.h"
#include "GameFramework/Actor.h"
#include "IslandMeshGenerator.generated.h"

UCLASS(Blueprintable, BlueprintType)
class PROCEDURALISLANDGENERATOR_API UIslandMeshGenerator : public UObject
{
	GENERATED_BODY()

public:
	UIslandMeshGenerator();

	UFUNCTION(BlueprintCallable, CallInEditor, Category="Generation")
	AIslandProceduralMesh* GenerateMesh(const TArray<FVector>& Vertices);

protected:
	UPROPERTY(EditDefaultsOnly, Category="Island")
	float Height = 5000;

	UPROPERTY(EditDefaultsOnly, Category="Materials")
	UMaterialInterface* TopIslandMaterial;

	UPROPERTY(EditDefaultsOnly, Category="Materials")
	UMaterial* SideIslandMaterial;

	UPROPERTY(EditDefaultsOnly, Category="IslandMesh")
	TSubclassOf<AIslandProceduralMesh> IslandMeshClass;

	UPROPERTY(VisibleInstanceOnly, Category="IslandMesh")
	AIslandProceduralMesh* Mesh;

	UFUNCTION()
	void GenerateTopFaces(const TArray<FVector>& Vertices) const;

	UFUNCTION()
	void GenerateSideFaces(const TArray<FVector>& Vertices) const;

};
