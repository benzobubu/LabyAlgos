#pragma once

#include "CoreMinimal.h"
#include "ProceduralMeshComponent.h"
#include "GameFramework/Actor.h"
#include "IslandProceduralMesh.generated.h"

UCLASS()
class PROCEDURALISLANDGENERATOR_API AIslandProceduralMesh : public AActor
{
	GENERATED_BODY()

public:
	AIslandProceduralMesh();

	UFUNCTION(BlueprintCallable, Category="ProceduralMeshComp")
	UProceduralMeshComponent* GetProceduralMeshComponent() const;

protected:
	UPROPERTY(VisibleAnywhere, Category="ProceduralMeshComp")
	UProceduralMeshComponent* IslandProcMeshComp;

};
