#pragma once

#include "CoreMinimal.h"
#include "ProceduralMeshComponent.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "ProceduralMeshUtilities.generated.h"

UCLASS()
class PROCEDURALISLANDGENERATOR_API UProceduralMeshUtilities : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	static TArray<FProcMeshVertex> ConvertVectorsToProcMeshVertices(const TArray<FVector>& Vertices);

};
