#include "ProceduralMeshUtilities.h"
#include "ProceduralMeshComponent.h"

TArray<FProcMeshVertex> UProceduralMeshUtilities::ConvertVectorsToProcMeshVertices(const TArray<FVector>& Vertices)
{
	TArray<FProcMeshVertex> ProcMeshVertices;

	for(const FVector Position : Vertices)
	{
		FProcMeshVertex Vertex;
		Vertex.Position = Position;
		ProcMeshVertices.Add(Vertex);
	}

	return ProcMeshVertices;
}
