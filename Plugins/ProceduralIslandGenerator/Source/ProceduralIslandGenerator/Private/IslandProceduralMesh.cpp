#include "IslandProceduralMesh.h"

AIslandProceduralMesh::AIslandProceduralMesh()
{
	IslandProcMeshComp = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("IslandProcMeshComp"));
	RootComponent = IslandProcMeshComp;
}

UProceduralMeshComponent* AIslandProceduralMesh::GetProceduralMeshComponent() const
{
	return IslandProcMeshComp;
}
