#include "IslandGenerator.h"
#include "IslandProceduralMesh.h"
#include "RandomPerimeterGenerator.h"

AIslandGenerator::AIslandGenerator()
{
}

void AIslandGenerator::GenerateIsland()
{
	RemoveCurrentIsland();

	if (IslandMeshGenerator == nullptr && IslandMeshGeneratorClass != nullptr)
	{
		IslandMeshGenerator = NewObject<UIslandMeshGenerator>(this, IslandMeshGeneratorClass);
	}

	if (IslandMeshGenerator != nullptr)
	{
		const float RadiusMin = MinimumSize / 2;
		const float RadiusMax = MaximumSize / 2;
		Vertices = URandomPerimeterGenerator::Generate(Seed, ShapeVerticesCount, RadiusMin, RadiusMax);
		AIslandProceduralMesh* IslandProceduralMesh = IslandMeshGenerator->GenerateMesh(Vertices);
		if (IslandProceduralMesh != nullptr)
		{
			IslandMesh = IslandProceduralMesh;
		}
	}
}

void AIslandGenerator::RemoveCurrentIsland()
{
	if (IslandMesh != nullptr)
	{
		IslandMesh->Destroy();
		IslandMesh = nullptr;
	}
}

void AIslandGenerator::SetSeed(const int32 NewSeed)
{
	Seed = NewSeed;
}

void AIslandGenerator::SetIslandMaxSize(const float Size)
{
	MinimumSize = Size;
}

void AIslandGenerator::SetIslandMinSize(const float Size)
{
	MaximumSize = Size;
}

TArray<FVector>& AIslandGenerator::GetIslandFloor()
{
	return Vertices;
}
