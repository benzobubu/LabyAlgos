#include "IslandMeshGenerator.h"
#include "IslandProceduralMesh.h"
#include "ProceduralMeshUtilities.h"

UIslandMeshGenerator::UIslandMeshGenerator()
{
}

AIslandProceduralMesh* UIslandMeshGenerator::GenerateMesh(const TArray<FVector>& Vertices)
{
	if (IslandMeshClass != nullptr)
	{
		Mesh = GetWorld()->SpawnActor<AIslandProceduralMesh>(IslandMeshClass, FTransform::Identity);
	}

	if (Mesh != nullptr)
	{
		const TArray<FProcMeshVertex> AreaProcMeshVertices = UProceduralMeshUtilities::ConvertVectorsToProcMeshVertices(Vertices);
		GenerateTopFaces(Vertices);
		GenerateSideFaces(Vertices);
		return Mesh;
	}

	return nullptr;
}

void UIslandMeshGenerator::GenerateTopFaces(const TArray<FVector>& Vertices) const
{
	const int AreaPoints = Vertices.Num();

	const FVector Center (FVector::ZeroVector);
	TArray<FVector> TopVertices;
	TopVertices.Add(Center);
	TopVertices.Append(Vertices);

	TArray<int32> Triangles;
	for (int i = 0; i < AreaPoints; i ++)
	{
		Triangles.Add(i + 1);
		Triangles.Add(0);
		Triangles.Add(i == AreaPoints - 1 ? 1 : i + 2);
	}

	UProceduralMeshComponent* ProcMeshComp = Mesh->GetProceduralMeshComponent();
	ProcMeshComp->CreateMeshSection(0, TopVertices, Triangles, TArray<FVector>(), TArray<FVector2D>(), TArray<FColor>(), TArray<FProcMeshTangent>(), true);

	if (TopIslandMaterial)
	{
		ProcMeshComp->SetMaterial(0, TopIslandMaterial);
	}
}

void UIslandMeshGenerator::GenerateSideFaces(const TArray<FVector>& Vertices) const
{
	const int AreaPoints = Vertices.Num();

	TArray<FVector> Bottom (Vertices);
	for(auto& Point : Bottom)
	{
		Point += FVector::UpVector * -Height;
	}

	TArray<FVector> SideVertices (Vertices);
	SideVertices.Append(Bottom);

	TArray<int32> Triangles;
	for (int i = 0; i < AreaPoints; i ++)
	{
		const int SquareVertex1 = i;
		const int SquareVertex2 = i + 1 < AreaPoints ? i + 1 : 0;
		const int SquareVertex3 = i + AreaPoints;
		const int SquareVertex4 = i + 1 + AreaPoints < (AreaPoints * 2) ? AreaPoints + i + 1 : AreaPoints;

		Triangles.Add(SquareVertex1);
		Triangles.Add(SquareVertex2);
		Triangles.Add(SquareVertex3);

		Triangles.Add(SquareVertex2);
		Triangles.Add(SquareVertex4);
		Triangles.Add(SquareVertex3);
	}

	UProceduralMeshComponent* ProcMeshComp = Mesh->GetProceduralMeshComponent();
	ProcMeshComp->CreateMeshSection(1, SideVertices, Triangles, TArray<FVector>(), TArray<FVector2D>(), TArray<FColor>(), TArray<FProcMeshTangent>(), true);

	if (SideIslandMaterial)
	{
		ProcMeshComp->SetMaterial(1, SideIslandMaterial);
	}
}
