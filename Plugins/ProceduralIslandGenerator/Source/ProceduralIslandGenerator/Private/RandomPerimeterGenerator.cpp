#include "RandomPerimeterGenerator.h"

URandomPerimeterGenerator::URandomPerimeterGenerator()
{
}

TArray<FVector> URandomPerimeterGenerator::Generate(const int32 Seed, const int PerimeterPointsCount, const float RadiusMinimum, const float RadiusMaximum)
{
	const FRandomStream RandomStream (Seed);
	const FVector Center (FVector::ZeroVector);
	const float Angle = 360 / PerimeterPointsCount;
	TArray<FVector> Perimeter;

	for(int i = 0; i < PerimeterPointsCount; i ++)
	{
		const float Radius = RandomStream.FRandRange(RadiusMinimum, RadiusMaximum);
		Perimeter.Add(GetPositionAtAngleAndDistance(Center, Angle * i, Radius));
	}

	return Perimeter;
}

FVector URandomPerimeterGenerator::GetPositionAtAngleAndDistance(const FVector Position, const float Angle, const float Distance)
{
	const float x = Position.X + FMath::Cos(FMath::DegreesToRadians(Angle)) * Distance;
	const float y = Position.Y + FMath::Sin(FMath::DegreesToRadians(Angle)) * Distance;
	return FVector(x, y, 0.f);
}
