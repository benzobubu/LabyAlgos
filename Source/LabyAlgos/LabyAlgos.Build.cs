// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class LabyAlgos : ModuleRules
{
	public LabyAlgos(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "ProceduralIslandGenerator", "ProceduralLabyrinthGenerator" });
		
		PrivateDependencyModuleNames.AddRange(new string[] { });

	}
}
