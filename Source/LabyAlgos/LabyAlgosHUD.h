// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "LabyAlgosHUD.generated.h"

UCLASS()
class ALabyAlgosHUD : public AHUD
{
	GENERATED_BODY()

public:
	ALabyAlgosHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

