#pragma once

#include "CoreMinimal.h"
#include "ProceduralIslandGenerator/Public/IslandGenerator.h"
#include "GameFramework/Actor.h"
#include "Generators/LabyrinthGenerator.h"
#include "ProceduralWorldGenerator.generated.h"

UCLASS()
class LABYALGOS_API AProceduralWorldGenerator : public AActor
{
	GENERATED_BODY()

public:
	AProceduralWorldGenerator();

	UFUNCTION(CallInEditor, BlueprintCallable)
	void RandomizeSeedAndGenerateWorld();

	UFUNCTION(CallInEditor, BlueprintCallable)
	void GenerateWorld();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Seed")
	int32 Seed;

	UFUNCTION(BlueprintCallable, Category="Seed")
	void RandomizeSeed();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Island")
	TSubclassOf<AIslandGenerator> IslandGeneratorClass;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category="Island")
	AIslandGenerator* IslandGenerator;

	UFUNCTION(BlueprintCallable, Category="Island")
	void GenerateIsland();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Labyrinth")
	TSubclassOf<ALabyrinthGenerator> LabyrinthGeneratorClass;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category="Labyrinth")
	ALabyrinthGenerator* LabyrinthGenerator;

	UFUNCTION(BlueprintCallable, Category="Labyrinth")
	void GenerateLabyrinth();
};
