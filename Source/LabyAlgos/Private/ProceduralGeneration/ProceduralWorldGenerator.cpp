#include "ProceduralGeneration/ProceduralWorldGenerator.h"

AProceduralWorldGenerator::AProceduralWorldGenerator()
{
}

void AProceduralWorldGenerator::RandomizeSeedAndGenerateWorld()
{
	RandomizeSeed();
	GenerateWorld();
}

void AProceduralWorldGenerator::GenerateWorld()
{
	GenerateIsland();
	GenerateLabyrinth();
}

void AProceduralWorldGenerator::RandomizeSeed()
{
	Seed = FMath::RandRange(0, INT32_MAX - 1);
}

void AProceduralWorldGenerator::GenerateIsland()
{
	if (IslandGenerator == nullptr && IslandGeneratorClass != nullptr)
	{
		IslandGenerator = GetWorld()->SpawnActor<AIslandGenerator>(IslandGeneratorClass, GetActorTransform());
	}

	if (IslandGenerator != nullptr)
	{
		IslandGenerator->SetSeed(Seed);
		IslandGenerator->GenerateIsland();
	}
}

void AProceduralWorldGenerator::GenerateLabyrinth()
{
	if (LabyrinthGenerator == nullptr && LabyrinthGeneratorClass != nullptr)
	{
		LabyrinthGenerator = GetWorld()->SpawnActor<ALabyrinthGenerator>(LabyrinthGeneratorClass, FTransform::Identity);
	}

	if (IslandGenerator != nullptr && LabyrinthGenerator != nullptr)
	{
		const TArray<FVector> IslandVertices = IslandGenerator->GetIslandFloor();
		LabyrinthGenerator->SetLabyrinthArea(IslandVertices);
		LabyrinthGenerator->SetSeed(Seed);
		LabyrinthGenerator->GenerateLabyrinth();
	}
}