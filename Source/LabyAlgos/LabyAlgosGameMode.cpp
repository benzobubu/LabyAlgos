// Copyright Epic Games, Inc. All Rights Reserved.

#include "LabyAlgosGameMode.h"
#include "LabyAlgosHUD.h"
#include "LabyAlgosCharacter.h"
#include "UObject/ConstructorHelpers.h"

ALabyAlgosGameMode::ALabyAlgosGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ALabyAlgosHUD::StaticClass();
}
